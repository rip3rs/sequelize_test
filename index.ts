import { Server } from './src/server'
import { IEnv } from 'src/@types/env.interface'
require('dotenv').config()

declare global {
  namespace NodeJS {
    interface ProcessEnv extends IEnv {}
  }
}

new Server()
