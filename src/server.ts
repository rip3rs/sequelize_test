import { sequelize } from '../config'
import { ServerCore } from './core'

export class Server extends ServerCore {
  constructor() {
    super()
    this.init()
  }

  private init(): void {
    sequelize()
      .sync({ force: false })
      .then(() => this.start())

    // FOR DEV ONLY
    // sequelize()
    //   .query('SET FOREIGN_KEY_CHECKS = 0')
    //   .then(() => sequelize().sync({ force: true }))
    //   .then(() => sequelize().query('SET FOREIGN_KEY_CHECKS = 1'))
    //   .then(res => this.start())
    //   .catch(e => {
    //     console.error('Squelize Sync Error', e)
    //     throw new Error('Something wrong happened when syncing Sequelize')
    //   })
  }

  private start(): void {
    this.app.listen(process.env.SERVER_PORT, () => console.log(`server working on port: ${process.env.SERVER_PORT}`))
  }
}
