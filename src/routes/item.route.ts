import { Router } from 'express'
import { RoutesCore } from '../core'
import { ItemController } from '../controllers'

export default class ItemRoute extends RoutesCore {
  public name: string = 'item'

  constructor() {
    super(true)

    this.controller = new ItemController()
    this.router = Router()

    this.defaultRoutes()
    this.customRoutes()
  }

  private customRoutes(): void {}
}
