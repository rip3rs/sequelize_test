import { Router } from 'express'
import { RoutesCore } from '../core'
import { AuthController } from '../controllers'

export default class AuthRoute extends RoutesCore {
  public name: string = 'auth'

  constructor() {
    super(false)

    this.controller = new AuthController()
    this.router = Router()

    this.customRoutes()
  }

  private customRoutes(): void {
    this.router.post(`/login`, async (req, res, next) => {
      try {
        const auth = await this.controller.login(req.body)
        switch (auth) {
          case 400:
            res.status(400).json({ message: 'No Email added' })
            break
          case 401:
            res.status(401).json({ message: 'No User with that Email' })
            break
          case 404:
            res.status(404).json({ message: 'Some error happened' })
            break

          default:
            res.status(200).json(auth)
            break
        }
      } catch (e) {
        console.error('#ROUTE# - Error login:', e)
        next(e)
      }
    })
  }
}
