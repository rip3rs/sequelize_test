import { Router } from 'express'
import { VanController } from '../controllers'
import { RoutesCore } from '../core'

export default class VanRoute extends RoutesCore {
  public name: string = 'van'

  constructor() {
    super(true)

    this.controller = new VanController()
    this.router = Router()

    this.defaultRoutes()
    this.customRoutes()
  }

  private customRoutes(): void {}
}
