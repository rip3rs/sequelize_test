import { Router } from 'express'
import { RoutesCore } from '../core'
import { ItemListController } from '../controllers'

export default class ItemListRoute extends RoutesCore {
  public name: string = 'item-list'

  constructor() {
    super(true)

    this.controller = new ItemListController()
    this.router = Router()

    this.defaultRoutes()
    this.customRoutes()
  }

  private customRoutes(): void {}
}
