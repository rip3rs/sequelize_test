import { Router } from 'express'
import { UserController } from '../controllers'
import { RoutesCore } from '../core'

export default class UserRoute extends RoutesCore {
  public name: string = 'user'

  constructor() {
    super(true)

    this.controller = new UserController()
    this.router = Router()

    this.defaultRoutes()
    this.customRoutes()
  }

  private customRoutes(): void {}
}
