import express from 'express'
import bodyParser from 'body-parser'
import { readDir, getSrcPath } from '../utils/fs'

export class ServerCore {
  protected app = express()

  constructor() {
    this.middlewares()
    this.routeList()
  }

  private async middlewares(): Promise<void> {
    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.app.use(bodyParser.json({ limit: '5mb' }))

    try {
      const routes: string[] = await readDir(`${getSrcPath()}/middlewares`)

      for (let i = 0; i < routes.length; i++) {
        const routeName: string = routes[i].split('.ts')[0]

        this.app.use(`/${process.env.PRIVATE_PATH}`, require(`../middlewares/${routeName}`))
      }
    } catch (e) {
      console.error(`Setting Middlewares ERROR:\n ${e}`)
      throw new Error('Something went wrong with setting Middlewares.')
    }
  }

  protected async routeList() {
    try {
      const routes: string[] = await readDir(`${getSrcPath()}/routes`)

      console.log('############## Available Routes')
      for (let i = 0; i < routes.length; i++) {
        const routeName: string = routes[i].split('.ts')[0]
        const routerModule = require(`../routes/${routeName}`)
        const router = new routerModule.default()
        console.log(`${router.private}/${router.name}`)
        this.app.use(`${router.private}/${router.name}`, router.router)
      }
      console.log('##############')
    } catch (e) {
      console.error(`Routes Generation ERROR:\n ${e}`)
      throw new Error('Something went wrong with generating routes.')
    }
  }
}
