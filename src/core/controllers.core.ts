export class ControllersCore<T> {
  private model: any

  constructor(model) {
    this.model = model
  }

  protected async findAll(): Promise<T[] | string> {
    try {
      return await this.model.findAll()
    } catch (e) {
      console.error('#CONTROLLER# - Error findAll:', e)
      return e
    }
  }

  protected async findOne(id: string): Promise<T | string> {
    try {
      return await this.model.findOne({ where: { id } })
    } catch (e) {
      console.error('#CONTROLLER# - Error findOne:', e)
      return e
    }
  }

  protected async create(data: T): Promise<T | string> {
    try {
      return await this.model.create(data)
    } catch (e) {
      console.error('#CONTROLLER# - Error create:', e)
      return e
    }
  }

  protected async update(id: number, data: Partial<T>): Promise<[number, T[]] | string> {
    try {
      return await this.model.update(data, { where: { id } })
    } catch (e) {
      console.error('#CONTROLLER# - Error update:', e)
      return e
    }
  }
}
