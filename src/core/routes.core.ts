export class RoutesCore {
  protected router: any
  protected controller: any
  protected private: string

  constructor(isPrivate = true) {
    this.private = isPrivate ? `/${process.env.PRIVATE_PATH}` : ''
  }

  protected defaultRoutes() {
    this.router.get('', async (req, res, next) => {
      try {
        res.json(await this.controller.findAll())
      } catch (e) {
        console.error('#ROUTE# - Error findAll:', e)
        next(e)
      }
    })

    this.router.get('/:id', async (req, res, next) => {
      try {
        const findOne = await this.controller.findOne(req.params['id'])
        res.json(findOne)
      } catch (e) {
        console.error('#ROUTE# - Error findOne:', e)
        next(e)
      }
    })

    this.router.post('', async (req, res, next) => {
      try {
        const create = await this.controller.create(req.body)
        res.status(201).json(create)
      } catch (e) {
        console.error('#ROUTE# - Error create:', e)
        next(e)
      }
    })

    this.router.put('/:id', async (req, res, next) => {
      try {
        await this.controller.update(req.params['id'], req.body)
        res.sendStatus(200)
      } catch (e) {
        console.error('#ROUTE# - Error update:', e)
        next(e)
      }
    })
  }
}
