import User from '../Models/User.model'
import jwt from 'jsonwebtoken'
import { jwtSign } from '../services/jwt'

export class AuthController {
  constructor() {}

  public async login(body): Promise<any> {
    try {
      const { email, password } = body

      if (!email) {
        return Promise.resolve(400)
      }

      const user = await User.findOne({ where: { email: email } })

      if (user) {
        return jwtSign(user)
      } else {
        return Promise.resolve(401)
      }
    } catch (e) {
      console.error('#CONTROLLER# - Error User Login:', e)
      return Promise.resolve(404)
    }
  }
}
