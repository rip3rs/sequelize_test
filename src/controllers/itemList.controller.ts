import { ControllersCore } from '../core'
import ItemList from '../Models/ItemList.model'
import Van from '../Models/Van.model'

export class ItemListController extends ControllersCore<ItemList> {
  constructor() {
    super(ItemList)
  }

  public async create(data: ItemList): Promise<ItemList> {
    try {
      let itemList = { ...data }
      const van = await Van.findOne({ where: { id: data.assignedVanId } })
      itemList.selectedVanTotalCapacity = van.totalCapacity

      return await ItemList.create<ItemList>(itemList)
    } catch (e) {
      console.error('#ITEM CONT# - Error custom createItem:', e)
      return e
    }
  }
}
