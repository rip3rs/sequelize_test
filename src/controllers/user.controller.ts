import User from '../Models/User.model'
import { ControllersCore } from '../core'

export class UserController extends ControllersCore<User> {
  constructor() {
    super(User)
  }
}
