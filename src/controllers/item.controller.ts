import { ControllersCore } from '../core'
import Item from '../Models/Item.model'
import Location from '../Models/Location.model'
import { googleMapsGeoLocationService } from '../services'
import ItemGeoLocation from '../Models/ItemGeoLocation.model'
import ItemList from '../Models/ItemList.model'

export class ItemController extends ControllersCore<Item> {
  constructor() {
    super(Item)
  }

  public async create(data: Item): Promise<Item | string> {
    try {
      const itemReq = { ...data }
      const { location } = data

      delete itemReq.location

      const item = await Item.create<Item>(data)
      const itemList = await ItemList.findOne({ where: { id: data.itemListId } })

      if (itemList.selectedVanTotalCapacity <= itemList.currentCapacity + data.size) {
        return Promise.resolve('List is at MAX capacity of the selected Van')
      }

      itemList.currentCapacity += data.size
      itemList.save()

      await this.createLocation(item.id, location)

      return await Item.findOne<Item>({
        where: { id: item.id },
        include: [{ model: Location, include: [ItemGeoLocation] }]
      })
    } catch (e) {
      console.error('#ITEM CONT# - Error custom createItem:', e)
      return e
    }
  }

  private async createLocation(itemId: number, data: Location): Promise<Location> {
    try {
      const location = await Location.create<Location>({ itemId: itemId, ...data })
      const geoLoc: any = await googleMapsGeoLocationService(data)
      let itemGeoLocation = { locationId: location.id }

      if (geoLoc.length > 0) {
        itemGeoLocation = { ...itemGeoLocation, ...geoLoc[0].geometry.location }
      }

      await ItemGeoLocation.create<ItemGeoLocation>(itemGeoLocation)

      return await Location.findOne<Location>({ where: { id: location.id }, include: [ItemGeoLocation] })
    } catch (e) {
      console.error('#ITEM CONT# - Error custom createLocation:', e)
      return e
    }
  }
}
