import { jwtVerify, jwtSign } from '../services/jwt'

module.exports = (req, res, next) => {
  if (req.hasOwnProperty('headers') && req.headers.hasOwnProperty('authorization')) {
    try {
      const jwtPayload = jwtVerify(<string>req.headers['authorization'])
      res.locals.jwtPayload = jwtPayload

      //send token again
      const newToken = jwtSign(jwtPayload)

      res.setHeader('authorization', newToken)
    } catch (err) {
      console.error('#Middleware# JWT Error: ', err)
      return res.status(401).json({
        error: {
          msg: 'Failed to authenticate token!'
        }
      })
    }
  } else {
    return res.status(401).json({
      error: {
        msg: 'No token!'
      }
    })
  }
  next()
  return
}
