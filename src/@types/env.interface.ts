export interface IEnv {
  SERVER_PORT: number
  PRIVATE_PATH: string
  DB_HOST: string
  DB_PORT: number
  DB_NAME: string
  DB_USERNAME: string
  DB_PASSWORD: string
  JWT_SECRET: string
}
