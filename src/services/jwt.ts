import jwt from 'jsonwebtoken'
import User from '../Models/User.model'

export const jwtSign = (user: User) =>
  jwt.sign({ userId: user.id, name: user.name, email: user.email, role: user.role }, process.env.JWT_SECRET, {
    expiresIn: '24h'
  })

export const jwtVerify = token => jwt.verify(token, process.env.JWT_SECRET)
