import Axios from 'axios'
import Location from '../Models/Location.model'

export const googleMapsGeoLocationService = (data: Location): Promise<any> => {
  return Axios.get('https://enzb1wq6hygforq.m.pipedream.net', {
    headers: {
      'x-pipedream-response': '1'
    },
    params: {
      address: data.address,
      address2: data.address2,
      zip: data.zip,
      district: data.district,
      country: data.country
    }
  }).then(res => res.data.results)
}
