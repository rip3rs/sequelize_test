import { readdir } from 'fs'
import path from 'path'

export const readDir = (pathToFolder): Promise<string[]> => {
  return new Promise((res, rej): string[] | Error | void =>
    readdir(pathToFolder, (err, files) => (err ? rej(err) : res(files)))
  )
}

export const getSrcPath = (): string => {
  return path.join(path.basename(path.dirname(__dirname)))
}
