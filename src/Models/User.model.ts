import { Table, Column, Model, HasOne, DataType } from 'sequelize-typescript'
import Van from './Van.model'

@Table({
  timestamps: true
})
export default class User extends Model<User> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column
  name: string

  @Column({
    unique: true
  })
  email: string

  @Column
  password: string

  @Column
  role: string

  @HasOne(() => Van)
  selectedVan: Van
}
