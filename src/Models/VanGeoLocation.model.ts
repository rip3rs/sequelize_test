import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Van from './Van.model'
import VanHistory from './VanHistory.model'

@Table({
  timestamps: false
})
export default class VanGeoLocation extends Model<VanGeoLocation> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column({
    type: DataType.FLOAT
  })
  lat: number

  @Column({
    type: DataType.FLOAT
  })
  lng: number

  @ForeignKey(() => Van)
  @ForeignKey(() => VanHistory)
  @Column({
    type: DataType.UUID
  })
  vanId: number

  @BelongsTo(() => Van)
  van: Van

  @BelongsTo(() => VanHistory)
  vanHistory: VanHistory
}
