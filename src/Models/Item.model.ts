import { Table, Column, Model, Default, HasOne, BelongsTo, ForeignKey, DataType } from 'sequelize-typescript'
import Location from './Location.model'
import ItemList from './ItemList.model'

@Table({
  timestamps: true
})
export default class Item extends Model<Item> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column
  name: string

  @Column
  size: number

  @Default(false)
  @Column
  fetched: boolean

  @ForeignKey(() => ItemList)
  @Column({
    type: DataType.UUID
  })
  itemListId: number

  @BelongsTo(() => ItemList)
  itemList: ItemList

  @HasOne(() => Location)
  location: Location
}
