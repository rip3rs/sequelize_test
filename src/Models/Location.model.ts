import { Table, Column, Model, AllowNull, HasOne, BelongsTo, DataType, ForeignKey } from 'sequelize-typescript'
import ItemGeoLocation from './ItemGeoLocation.model'
import Item from './Item.model'

@Table({
  timestamps: false
})
export default class Location extends Model<Location> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column
  address: string

  @AllowNull
  @Column
  address2: string

  @Column
  zip: string

  @Column
  district: string

  @Column
  country: string

  @AllowNull
  @Column
  telephone: string

  @ForeignKey(() => Item)
  @Column({
    type: DataType.UUID
  })
  itemId: number

  @HasOne(() => ItemGeoLocation)
  geoLocation: ItemGeoLocation

  @BelongsTo(() => Item)
  item: Item
}
