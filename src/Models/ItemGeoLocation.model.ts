import { Table, Column, Model, DataType, BelongsTo, ForeignKey } from 'sequelize-typescript'
import Location from './Location.model'

@Table({
  timestamps: false
})
export default class ItemGeoLocation extends Model<ItemGeoLocation> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @ForeignKey(() => Location)
  @Column({
    type: DataType.UUID
  })
  locationId: number

  @Column({
    type: DataType.FLOAT
  })
  lat: number

  @Column({
    type: DataType.FLOAT
  })
  lng: number

  @BelongsTo(() => Location)
  location: Location
}
