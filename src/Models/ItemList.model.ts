import { Table, Column, Model, HasMany, BelongsTo, ForeignKey, DataType } from 'sequelize-typescript'
import Item from './Item.model'
import Van from './Van.model'

@Table({
  timestamps: true
})
export default class ItemList extends Model<ItemList> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column
  name: string

  @Column({
    defaultValue: 0
  })
  currentCapacity: number

  @Column({
    defaultValue: 0
  })
  selectedVanTotalCapacity: number

  @ForeignKey(() => Van)
  @Column({
    type: DataType.UUID
  })
  assignedVanId: number

  @HasMany(() => Item)
  items: Item[]

  @BelongsTo(() => Van)
  van: Van
}
