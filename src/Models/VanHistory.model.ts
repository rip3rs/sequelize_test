import { Table, Column, Model, BelongsTo, HasMany, DataType, ForeignKey } from 'sequelize-typescript'
import Van from './Van.model'
import VanGeoLocation from './VanGeoLocation.model'

@Table({
  timestamps: true
})
export default class VanHistory extends Model<VanHistory> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @ForeignKey(() => Van)
  @Column({
    type: DataType.UUID
  })
  vanId: number

  @HasMany(() => VanGeoLocation)
  locationHistory: VanGeoLocation[]

  @BelongsTo(() => Van)
  assignedVan: Van
}
