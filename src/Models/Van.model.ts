import { Table, Column, Model, HasOne, HasMany, ForeignKey, BelongsTo, DataType } from 'sequelize-typescript'
import User from './User.model'
import ItemList from './ItemList.model'
import VanHistory from './VanHistory.model'
import VanGeoLocation from './VanGeoLocation.model'

@Table({
  timestamps: true
})
export default class Van extends Model<Van> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: DataType.UUIDV4
  })
  id: number

  @Column
  totalCapacity: number

  @Column
  numberPlate: string

  @Column
  color: string

  @HasOne(() => VanGeoLocation)
  currentLocation: VanGeoLocation

  @HasOne(() => ItemList)
  itemList: ItemList

  @HasMany(() => VanHistory)
  history: VanHistory[]

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID
  })
  assignedToId: number

  @BelongsTo(() => User)
  selectedUser: User
}
