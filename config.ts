import { Sequelize } from 'sequelize-typescript'
import mysql2 from 'mysql2'

export const sequelize = () => {
  return new Sequelize({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    dialect: 'mysql',
    dialectModule: mysql2,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    models: [__dirname + '/src/Models']
  })
}
