-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: sequelize_test
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ItemGeoLocations`
--

DROP TABLE IF EXISTS `ItemGeoLocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ItemGeoLocations` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `locationId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `locationId` (`locationId`),
  CONSTRAINT `ItemGeoLocations_ibfk_1` FOREIGN KEY (`locationId`) REFERENCES `Locations` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ItemGeoLocations`
--

LOCK TABLES `ItemGeoLocations` WRITE;
/*!40000 ALTER TABLE `ItemGeoLocations` DISABLE KEYS */;
INSERT INTO `ItemGeoLocations` VALUES ('25d3595c-35cb-49e0-8517-696ac9e6e3aa','f6f90324-c231-43ab-9762-74bf0ba5a7a8',37.028,-7.83777),('6680513e-a06d-4c74-ada0-340b345bf917','10dae349-cddd-41d8-b223-5d7e69d17bc4',37.028,-7.83777);
/*!40000 ALTER TABLE `ItemGeoLocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ItemLists`
--

DROP TABLE IF EXISTS `ItemLists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ItemLists` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `currentCapacity` int(11) DEFAULT '0',
  `selectedVanTotalCapacity` int(11) DEFAULT '0',
  `assignedVanId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `assignedVanId` (`assignedVanId`),
  CONSTRAINT `ItemLists_ibfk_1` FOREIGN KEY (`assignedVanId`) REFERENCES `Vans` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ItemLists`
--

LOCK TABLES `ItemLists` WRITE;
/*!40000 ALTER TABLE `ItemLists` DISABLE KEYS */;
INSERT INTO `ItemLists` VALUES ('022e93ec-b5be-4c59-aff2-1464efa20cc5','List 1',300,400,'395f1e43-dfc9-4583-a275-c374fbdf9499','2019-12-22 17:19:17','2019-12-22 17:20:37');
/*!40000 ALTER TABLE `ItemLists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Items`
--

DROP TABLE IF EXISTS `Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Items` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `fetched` tinyint(1) DEFAULT '0',
  `itemListId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `itemListId` (`itemListId`),
  CONSTRAINT `Items_ibfk_1` FOREIGN KEY (`itemListId`) REFERENCES `ItemLists` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Items`
--

LOCK TABLES `Items` WRITE;
/*!40000 ALTER TABLE `Items` DISABLE KEYS */;
INSERT INTO `Items` VALUES ('4cf27a94-5f2f-44d6-95ce-fe5679b127dd','FEDEX Box 3',150,0,'022e93ec-b5be-4c59-aff2-1464efa20cc5','2019-12-22 17:20:42','2019-12-22 17:20:42'),('8871b045-182b-4393-977d-7ab2fd801ba0','FEDEX Box 2',150,0,'022e93ec-b5be-4c59-aff2-1464efa20cc5','2019-12-22 17:20:37','2019-12-22 17:20:37'),('a110244f-b835-4883-8007-a04b19199415','FEDEX Box 1',150,0,'022e93ec-b5be-4c59-aff2-1464efa20cc5','2019-12-22 17:20:28','2019-12-22 17:20:28');
/*!40000 ALTER TABLE `Items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Locations`
--

DROP TABLE IF EXISTS `Locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Locations` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `itemId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `Locations_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `Items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Locations`
--

LOCK TABLES `Locations` WRITE;
/*!40000 ALTER TABLE `Locations` DISABLE KEYS */;
INSERT INTO `Locations` VALUES ('10dae349-cddd-41d8-b223-5d7e69d17bc4','Rua Gil Eanes','4, 4R','8700-474','Faro District','Portugal',NULL,'a110244f-b835-4883-8007-a04b19199415'),('f6f90324-c231-43ab-9762-74bf0ba5a7a8','Rua Gil Eanes','4, 4R','8700-474','Faro District','Portugal',NULL,'8871b045-182b-4393-977d-7ab2fd801ba0');
/*!40000 ALTER TABLE `Locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES ('4e14f9b2-c312-46ae-8056-7d2b4fb0116b','Joseph Briggs','jbriggsr3@gmail.com','123456','admin','2019-12-22 16:51:39','2019-12-22 16:51:39'),('ba06b3ca-ff39-4040-8417-3e4f2e8dddb3','Christian Giordano',NULL,NULL,'admin','2019-12-22 17:21:22','2019-12-22 17:21:22');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VanGeoLocations`
--

DROP TABLE IF EXISTS `VanGeoLocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VanGeoLocations` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `vanId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `vanId` (`vanId`),
  CONSTRAINT `VanGeoLocations_ibfk_1` FOREIGN KEY (`vanId`) REFERENCES `Vans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VanGeoLocations`
--

LOCK TABLES `VanGeoLocations` WRITE;
/*!40000 ALTER TABLE `VanGeoLocations` DISABLE KEYS */;
/*!40000 ALTER TABLE `VanGeoLocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VanHistories`
--

DROP TABLE IF EXISTS `VanHistories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VanHistories` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `vanId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `vanId` (`vanId`),
  CONSTRAINT `VanHistories_ibfk_1` FOREIGN KEY (`vanId`) REFERENCES `Vans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VanHistories`
--

LOCK TABLES `VanHistories` WRITE;
/*!40000 ALTER TABLE `VanHistories` DISABLE KEYS */;
/*!40000 ALTER TABLE `VanHistories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vans`
--

DROP TABLE IF EXISTS `Vans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Vans` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `totalCapacity` int(11) DEFAULT NULL,
  `numberPlate` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `assignedToId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `assignedToId` (`assignedToId`),
  CONSTRAINT `Vans_ibfk_1` FOREIGN KEY (`assignedToId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vans`
--

LOCK TABLES `Vans` WRITE;
/*!40000 ALTER TABLE `Vans` DISABLE KEYS */;
INSERT INTO `Vans` VALUES ('0b7e3d1d-4dd2-463f-a1e9-e87632873fda',400,'XX-X-XX','Gray',NULL,'2019-12-22 17:07:12','2019-12-22 17:07:12'),('395f1e43-dfc9-4583-a275-c374fbdf9499',400,'XX-X-XX','Gray',NULL,'2019-12-22 17:14:50','2019-12-22 17:14:50');
/*!40000 ALTER TABLE `Vans` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-22 17:24:21
